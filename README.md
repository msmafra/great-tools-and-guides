# Great Tools and Guides
## Scripts, tools, guides and other helpful stuff

> Links more directly related to Bash are on:
  [Bash Best Practices References](https://gitlab.com/msmafra/bash-best-practices-references)

### Great tools to use (most on Linux)
* [Accessing_X_from_a_remote_Machine_on_your_LAN](https://wiki.archlinux.org/index.php/XDMCP#Accessing_X_from_a_remote_Machine_on_your_LAN)
* [Fedora Ultimate Setup Scripts by David-Else](https://github.com/David-Else/fedora-ultimate-setup-script)
* [Nvidia Auto Installer for Fedora Workstation](https://github.com/msmafra/nvidia-auto-installer-for-fedora-ws)

### Gnome
* [Gnome Shell Extension Installer](https://github.com/brunelli/gnome-shell-extension-installer)

### Linux docs, commands, info, tools etc.
- [How To Configure Your Monitors With Xrandr in Linux - LinuxConfig.org](https://linuxconfig.org/how-to-configure-your-monitors-with-xrandr-in-linux#h6-conclusion)
- [inotify - get your file system supervised](https://inotify.aiken.cz/?section=incron&page=doc&lang=en)
- [/etc/os-release openSUSE](https://en.opensuse.org/SDB:Find_openSUSE_version)
- [Awk Tutorial - Tutorialspoint](https://www.tutorialspoint.com/awk/)
- [AWK Vs NAWK Vs GAWK](https://www.thegeekstuff.com/2011/06/awk-nawk-gawk/)
- [Nano config](https://nano-editor.org/dist/latest/nanorc.5.html)
- [crontab guru](https://crontab.guru/)
- [Red Hat's Battery Life Tool Kit](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/power_management_guide/bltk)
- [Red Hat's Example — Laptop](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/power_management_guide/example_laptop)
- [Epoch timestamp to human readable](https://unix.stackexchange.com/questions/2987/how-do-i-convert-an-epoch-timestamp-to-a-human-readable-format-on-the-cli#2993)
- [Linux append text to end of file](https://www.cyberciti.biz/faq/linux-append-text-to-end-of-file/)
- [What color codes can I use in my PS1 prompt?](https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt)
- [Linux xargs command](https://www.computerhope.com/unix/xargs.htm)
- [Check RAM Slots](https://appuals.com/check-ram-slots-linux/)
- [wttr.in is a console-oriented weather forecast service](https://github.com/chubin/wttr.in)
- [x2x allows the keyboard, mouse on one X display to be used to control another X display. ](https://github.com/dottedmag/x2x)
- [Replace GURB2 with Systemd-boot](https://kowalski7cc.xyz/blog/systemd-boot-fedora-32)
- [Gentoo - Efibootmgr](https://wiki.gentoo.org/wiki/Efibootmgr)
- [Ventoy, multiple Linux Live media in the same USB stick](https://github.com/ventoy/Ventoy)
- [Ventoy, create multiboot](https://www.ostechnix.com/how-to-create-multiboot-usb-drives-with-ventoy-in-linux/)

### OpenBSD, \*BSDs
- [OpenBSD doas manual page](https://man.openbsd.org/doas)
- [OpenBSD doas.conf manual page](https://man.openbsd.org/doas.conf)
- [Executing Commands as Another User - doas](https://www.openbsd.org/faq/faq10.html#doas)
- [DBUS specifications](https://dbus.freedesktop.org/doc/dbus-specification.html#term-bus-name)

### Guidelines, semantics etc.

- [HTML URL Encoding Reference](https://www.w3schools.com/tags/ref_urlencode.asp)
- [Semantic Versioning 2.0.0](https://semver.org/)
- [Linux File System: Chapter 3. The Root Filesystem](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03.html)
- [Single responsibility principle](https://en.wikipedia.org/wiki/Single_responsibility_principle)
- [DuckDuckGo vs Google](https://www.sebastianurban.dev/blog/5-reasons-why-duckduckgo-is-better-than-google-for-developers)
- [](https://amish.naidu.dev/blog/dbus/)

### Regular expressions (regex)

- [Regular-Expressions.info - Regex Tutorial, Examples and Reference - Regexp Patterns](https://www.regular-expressions.info/)
- [Regular Expressions 101: Regex in C++11 | | InformIT](https://www.informit.com/articles/article.aspx?p=2079020)
- [Regular expression with GREP to find duplicated characters](https://unix.stackexchange.com/questions/70933/regular-expression-for-finding-double-characters-in-bash)
- [SED to insert a line above the patern](https://stackoverflow.com/questions/11694980/using-sed-insert-a-line-above-or-below-the-pattern)
- [How to use sed to find and replace text in files in Linux / Unix shell](https://www.cyberciti.biz/faq/how-to-use-sed-to-find-and-replace-text-in-files-in-linux-unix-shell/)
- [Regular expression Basic vs Extended](https://www.gnu.org/software/grep/manual/html_node/Basic-vs-Extended.html)

### Git helpful commands etc.

- [Syncing a fork - GitHub Help](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork)
- [Configuring a remote for a fork - GitHub Help](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/configuring-a-remote-for-a-fork)
- [Pushing commits to a remote repository - GitHub Help](https://help.github.com/en/github/using-git/pushing-commits-to-a-remote-repository)
- [Keeping a fork up to date · GitHub](https://gist.github.com/CristinaSolana/1885435)
- [Keeping A GitHub Fork Updated](https://thoughtbot.com/blog/keeping-a-github-fork-updated)
- [How to Use GitLab and GitHub Together](https://steveperkins.com/migrating-projects-from-github-to-gitlab/)
- [Combine Multiple Commits](https://www.w3docs.com/snippets/git/how-to-combine-multiple-commits-into-one-with-3-steps.html)
- [bash last day of each month](https://stackoverflow.com/questions/12381501/how-to-use-bash-to-get-the-last-day-of-each-month-for-the-current-year-without-u)

### Markdown

- [Markdown Tables generator - TablesGenerator.com](https://www.tablesgenerator.com/markdown_tables)
- [Markdown Guide - Basic Syntax](https://www.markdownguide.org/basic-syntax)

### Themes etc.

- [adi1090x's Plymouth themes](https://github.com/msmafra/plymouth-themes);
- [Emojipedia](https://emojipedia.org/search/?q=computer)
- [Gnome/GTK Themes](https://itsfoss.com/best-gtk-themes/)

### Finding a Linux distribution

- [Distrochooser](https://distrochooser.de/en)
- [FindMeADistro on Reddit](https://www.reddit.com/r/FindMeADistro/)

### Rsync

- [Rsync to EXFAT Drive](https://www.scivision.dev/rsync-to-exfat-drive/)

### BTRFS

- [Richard's Brown # Creating openSUSE-style btrfs root partition & subvolumes](https://rootco.de/2018-01-19-opensuse-btrfs-subvolumes/)
- [Arch Linux - Snapper](https://wiki.archlinux.org/index.php/Snapper)
- [Arch Linux - Btrfs](https://wiki.archlinux.org/index.php/Btrfs)
- [Scripts for btrfs maintenance tasks like periodic scrub, balance, trim or defrag on selected mountpoints or directories](https://github.com/kdave/btrfsmaintenanc)
- [BTRFS out of space emergency response](https://ohthehugemanatee.org/blog/2019/02/11/btrfs-out-of-space-emergency-response/)
- [Monitoring script](https://www.reddit.com/r/btrfs/comments/ghayaw/monitoring_script_with_alert_sent_by_email/)

### ICONS

- [Primer -> Octicons](https://primer.style/octicons/)
- [Font Awesome](https://fontawesome.com/icons?d=gallery&m=free)

### Proton / Gaming on Linux :)

- [Proton](https://github.com/ValveSoftware/Proton/releases)
- [Proton FAQ](https://github.com/ValveSoftware/Proton/wiki/Proton-FAQ)
- [Valve Proton/Steam Proton](https://github.com/ValveSoftware/Proton)
- [Proton GE](https://github.com/GloriousEggroll/proton-ge-custom)
- [Kernel GE - kernel fsync](https://copr.fedorainfracloud.org/coprs/gloriouseggroll/kernel/)
- [MangoHUD](https://github.com/flightlessmango/MangoHud)
- [Fedora Kernel with Fsync](https://copr.fedorainfracloud.org/coprs/gloriouseggroll/kernel/)

### Flatpak, Snap and AppImage

- [Flatseal - flatpak apps permissions](https://www.ostechnix.com/how-to-easily-configure-flatpak-apps-permissions-with-flatseal/)
- [Flatseal Git](https://github.com/tchx84/flatseal)
- [Tauon Music Box]()

### Pulseaudio

- [Cadmus graphical background noise removal tool](https://github.com/josh-richardson/cadmus/)
- [Real-time Noise Suppression Plugin](https://github.com/werman/noise-suppression-for-voice)

### VM
- [QEMU Windows guest](https://wiki.gentoo.org/wiki/QEMU/Windows_guest)
- [Martin Wimpress](https://github.com/wimpysworld/quickemu)

### Stream, Other

- [EmojiPedia](https://emojipedia.org/)

### Gnome
* [Gnome Shell Extension Installer](https://github.com/brunelli/gnome-shell-extension-installer)

### Samba
* [Samba Wiki Documentation](https://wiki.samba.org/index.php/User_Documentation)
* [Fedora 30 Samba AD DC Guide](https://forum.level1techs.com/t/oo-os-fedora-30-samba-ad-dc-guide/149475)

### Other
* [Vault depoyment](https://learn.hashicorp.com/vault/getting-started/deploy)
